//Data End Point
BUSINESSENDPOINT = "data/"; // End Point

VERSIONNAME = 'V1.0';
var curY = new Date();
var yearNow = curY.getFullYear();
COPYRIGHTSYEAR = yearNow;
(function() {
     'use strict';
      var BUSINESS_SERVICE = BUSINESSENDPOINT;
      angular.module('rokkerLaps').constant('rokkerLapsConfig', { 
          viewBaseUrl: 'views/',
          services: {
              activityData :{ url: BUSINESS_SERVICE + 'activity-data.json' , name: 'Activity Data'}              
            },
          error_messages: {
              ServiceNotWorking: 'Service Error Try' 
          },
          versionNo: VERSIONNAME,
          copyrightsYear: COPYRIGHTSYEAR
      });
})();

