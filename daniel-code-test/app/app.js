(function() {
    'use strict';

    angular.module('rokkerLaps', ['ui.router','chart.js'])  
           .config(config)
           .run(init);

    config.$inject = ['$stateProvider', '$urlRouterProvider', 'rokkerLapsConfig','rokkerLapsTemplateProvider', '$locationProvider'];

    function config($stateProvider, $urlRouterProvider, rokkerLapsConfig, rokkerLapsTemplateProvider,$locationProvider){
        $locationProvider.hashPrefix('');
        //Default routs
        $urlRouterProvider.otherwise('/404');
        $urlRouterProvider.when('', '/dashboard');
        $urlRouterProvider.when('/', '/dashboard');

        $stateProvider.state('layout', {
            abstract: true,
            views: {
                mainContent: {
                    templateUrl: rokkerLapsTemplateProvider.template('common/layout.html')
                }
            }
        })
        .state('layout.appLayout', {
            parent: 'layout',
            views: {
                leftContanier: {
                    templateUrl: rokkerLapsTemplateProvider.template('common/left-container.html'),
                },

                mainContainer: {
                    templateUrl: rokkerLapsTemplateProvider.template('common/application-layout.html'),
                },

                footerContainer: {
                    templateUrl: rokkerLapsTemplateProvider.template('common/footer-container.html')
                }
            },
        })
        .state('layout.appLayout.dashboard', {
            url:'/dashboard',
            parent: 'layout.appLayout',
            views: {
                topContainer: {
                    templateUrl: rokkerLapsTemplateProvider.template('common/top-container.html')
                },

                activityContainer: {
                    templateUrl: rokkerLapsTemplateProvider.template('dashboard/index.html'),
                    controller: 'dashboardCtrl'
                }
            },
        })
        .state('layout.appLayout.news', {
            url:'/news',
            parent: 'layout.appLayout',
            views: {
                topContainer: {
                    templateUrl: rokkerLapsTemplateProvider.template('common/top-container.html')
                },

                activityContainer: {
                    templateUrl: rokkerLapsTemplateProvider.template('news/index.html')
                }
            },
        })
    }

    init.$inject = ['$rootScope','$location','$log','$templateCache','rokkerLapsConfig'];
    function init($rootScope,$location,$log,$templateCache,rokkerLapsConfig){

        //Remove all local template caches
        $templateCache.removeAll();
    }

})();

angular.module('rokkerLaps').run(function($rootScope) {
    $rootScope.safeApply = function(fn) {
        var phase = $rootScope.$$phase;
        if (phase === '$apply' || phase === '$digest') {
            if (fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };
});
