(function() {
    'use strict';
    angular
        .module('rokkerLaps')
        .factory('DataService', DataService);

    DataService.$inject = ['$q','$http','$state','rokkerLapsConfig']

    function DataService($q,$http,$state,rokkerLapsConfig) {
        return {
            getactivity: function(jsonName) {
                var jsonDataPath = rokkerLapsConfig.services[jsonName];
                var deferred = $q.defer();
                $http.get(jsonDataPath.url).then(function(data){
                   deferred.resolve(data);
                },function(error){
                    deferred.reject();
                });
                return deferred.promise;
            }
        }
    }

})();
