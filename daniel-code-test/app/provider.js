
angular.module('rokkerLaps').provider('rokkerLapsTemplate', ["rokkerLapsConfig", function rokkerLapsTemplate(rokkerLapsConfig) {

    var provider = this;
    
    this.template = function(url) {
        return function($stateParams) {
            var d = new Date();
            var n = d.getTime();
            return rokkerLapsConfig.viewBaseUrl + url + "?_v="+provider.handler.version+"&_t="+n;
        }
    }

    this.defaultTemplate = function(url) {
        return function($stateParams) {
            var d = new Date();
            var n = d.getTime();
            return rokkerLapsConfig.viewBaseUrl + url + "?_v="+provider.handler.version+"&_t="+n;
        }
    }

    this.updateConfig = function(key, value) {
        provider.handler[key] = value;
    }
    
    function Ver() {
        this.version = 0;
        this.language = null;
    }

    Ver.prototype = {

        selectedVersion: function(version) {
            this.version = version;
        },
        
        template: function(url){
            return provider.template(url);
        },

        defaultTemplate: function(url){
            return provider.defaultTemplate(url);
        }
    }

    provider.handler = new Ver();


    this.$get = [function Version() {
        console.log('Version initialized.')
        return provider.handler;
    }];

}])